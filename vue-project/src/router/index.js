import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue')
    },
    {
      path: '/lieux',
      name: 'lieux',
      component: () => import('../views/LieuxView.vue')
    },
    {
      path: '/inscription',
      name: 'inscription',
      component: () => import('../views/InscriptionView.vue')
    },
    {
      path: '/connexion',
      name: 'connexion',
      component: () => import('../views/ConnexionView.vue')
    },
    {
      path: '/routes/new',
      name: 'nouvelleVoie',
      component: () => import('../views/NouvelleVoieView.vue')
    },
    {
      path: '/areas/new',
      name: 'nouveauLieu',
      component: () => import('../views/NouveauLieuView.vue')
    },
    {
      path: '/areas/:areaId?',
      name: 'AreaSingle',
      component: () => import('../views/AreaSingleView.vue')
    },
    {
      path: '/routes/:routeId?',
      name: 'RouteSingle',
      component: () => import('../views/RouteSingleView.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import('../views/ProfileView.vue')
    },
    {
      path: '/areas/:areaId?',
      name: 'modifyArea',
      component: () => import('../views/NouveauLieuView.vue')
    },
    {
      path: '/routes/:routeId?',
      name: 'modifyRoute',
      component: () => import('../views/NouvelleVoieView.vue')
    }
  ]
})

export default router
