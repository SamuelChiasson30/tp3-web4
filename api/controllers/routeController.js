"use strict";

const Route = require("../models/route");
const dotenv = require("dotenv");
const util = require("./utils");

dotenv.config();

const url_base = process.env.URL + ":" + process.env.PORT;

exports.createRoute = async (req, res, next) => {
	try {
		const { name, type, grade, description, approach, descent, area} = req.body
		const user = req.user.id
		
		let newRoute = new Route({
			name: name,
			type: type,
			description: description,
			grade: grade,
			approach: approach,
			descent: descent,
			area: area,
			user: user
		})
		newRoute = await newRoute.save()
		res.location("/routes/" + newRoute.id)
		res.status(201).json(newRoute);
	}
	catch (err) {
		next(err);
	}
};


exports.getRoutes = async (req, res, next) => {
	try {
		let routes
		if(Object.keys(req.query).length > 0){
			const {type, gradeMin, gradeMax, area} = req.query;
			routes = await Route.find({type:type, "grade.value":{$gte: gradeMin, $lte: gradeMax},area: area});
		}else{
			routes = await Route.find()
		}
		if(routes === null){
			util.throwErreur(404, "aucune routes");
		}

		const routesHATEOAS = routes.map(route => {
			return{
				...route._doc,
				_links: [{
					self: {
						method: "GET",
						href: url_base + req.url + "/" + route._id.toString()
					},
					update: {
						method: "PUT",
						href: url_base + req.url + "/" + route._id.toString()
					},
					delete: {
						method: "DELETE",
						href: url_base + req.url + "/" + route._id.toString()
					}
				}]
			}
		})

		res.status(200).json(routesHATEOAS);

	}catch (err) {
		next(err);
	}
};

exports.getUserRoutes = async (req, res, next) => {
	Route.find({ user: req.user.id })
		.then(routes => {
			res.status(200).json(routes);
		})
		.catch(err => {
			next(err);
		});
};

exports.getRoute = (req, res, next) => {
	Route.findById(req.params.routeId)
		.then(route => {
			util.validateNull(route, "route inexistante", 404);
			res.status(200).json(route);
		})
		.catch(err => {
			next(err);
		});
};

exports.updateRoute = async (req, res, next) => {
	try {
		const { name, type, grade, description, approach, descent, area } = req.body
		const user = req.user.id
		const route = await Route.findByIdAndUpdate(req.params.routeId, { name: name, type: type, description: description, grade: grade, approach: approach, descent: descent, area: area, user: user }, { new: true })
		util.validateNull(route, "route inexistante", 404);
		res.status(200).json(route);
	}
	catch (err) {
		next(err);
	}

}

exports.deleteRoute = async (req, res, next) => {
	try {
		const route = await Route.deleteOne({ _id: req.params.routeId })
		res.status(204).json(route);
	}
	catch (err) {
		next(err);
	}
}




