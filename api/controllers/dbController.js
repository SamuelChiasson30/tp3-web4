"use strict";

const User = require("../models/user");
const Route = require("../models/route");
const Area =  require("../models/area");

const user_seeds = require("../seeds/users");
const routes_seeds = require("../seeds/routes");
const area_seeds = require("../seeds/areas");


let result = {};

exports.seed = async (req, res, next) =>{

    await User.deleteMany({})
        .then(() => User.insertMany(user_seeds))
        .then(() => Route.deleteMany({}))
        .then(() => Route.insertMany(routes_seeds))
        .then(() => Area.deleteMany({}))
        .then(() => Area.insertMany(area_seeds))
        .then(() => {
            res.send("Base de données réinitialisée avec succès !");
        })
    .catch(err => {
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err);
    });


   

};