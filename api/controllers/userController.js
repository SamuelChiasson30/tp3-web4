"use strict";

const User = require("../models/user");
const bcrypt = require("bcrypt");
const dotenv = require("dotenv");
const util = require("./utils");

dotenv.config();

const url_base = process.env.URL + ":" + process.env.PORT;

exports.createUser = async (req, res, next) => {
	const { username, email, password } = req.body
	try {
		const hashPassword = await bcrypt.hash(password, 5);

		if (username.length == 0) {
			util.throwErreur(400, "nom vide");
		}else if(username.length < 3 || username.length > 50){
			util.throwErreur(422, "nom invalide");
		}else if (password.length == 0){
			util.throwErreur(400, "mot de passe vide");
		}else if(password.length < 6){
			util.throwErreur(422, "mot de passe invalide");
		}else if (email.length > 50 || await User.exists({ email: email })) {
			util.throwErreur(422, "email invalide");
		}else if(email.length == 0){
			util.throwErreur(400, "email vide");
		}

		let newUser = new User({
			username: username,
			email: email,
			password: hashPassword
		})
		newUser = await newUser.save()
		res.location("/users/" + newUser.id);
		res.status(201).json(newUser);
	} catch (err) {
		next(err);
	}
};



exports.getUser = async (req, res, next) => {
	try {
		const user = await User.findById(req.params.id);
		if (user === null) {
			util.throwErreur(404, "utilisateur inexistant");
		}
		res.status(200).json(user, [
			{
				self: {
					method: "GET",
					href:url_base + req.url + "/" + user._id.toString()
				}
			},
		])

	} catch (err) {
		next(err);
	}
};
