"use strict";
const util = require("./utils");
const dotenv = require("dotenv");
const jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");
dotenv.config();

const User = require("../models/user");

exports.logIn = async (req, res, next) => {
    const { courriel, password } = req.body
    try {
        const user = await User.findOne({ email: courriel });
        util.validateNull(user, "nom d'utilisateur ou mot de passe invalide", 401);

        const mdp = await bcrypt.compare(password, user.password);
        if (!mdp) {
            util.throwErreur(401, "nom d'utilisateur ou mot de passe invalide");
        }

        const token = jwt.sign(
            {
                id: user._id
            },
            process.env.SECRET_JWT,
            { expiresIn: '24h' }
        );
        res.status(201).json({Token: token, username: user.username});
    }
    catch (err) {
        next(err);
    }

};


