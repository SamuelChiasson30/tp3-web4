'use strict';

//fonction pour vérifier si la variable entré est null et 
//envoyer un message d'erreur personnalisé si c'est le cas. 
exports.validateNull = (variable, message, code) => {
    if (variable == null) {
        const error = new Error(message);
        error.statusCode = code;
        throw error;
    }
}

exports.throwErreur = (erreur , msg) =>{
    const error = new Error(msg);
    error.statusCode = erreur;
    throw error;
}

exports.throwPartExist = () => {
    const error = new Error("participation existante");
    error.statusCode = 409;
    throw error;
}