"use strict";

const Area = require("../models/area");
const Route = require("../models/route");
const dotenv = require("dotenv");
const util = require("./utils");
dotenv.config();

const url_base = process.env.URL + ":" + process.env.PORT;

exports.createArea = async (req, res, next) => {
	try {
		const { name, description, gettingThere, lon, lat } = req.body
		const user = req.user.id
		
		let newArea = new Area({
			name: name,
			description: description,
			gettingThere: gettingThere,
			lon: lon,
			lat: lat,
			user: user
		})
		newArea = await newArea.save()
		res.location("/areas/" + newArea._id)
		res.status(201).json(newArea);
	}
	catch (err) {
		next(err);
	}
};


exports.getAreas = async (req, res, next) => {
	try {
		const areas = await Area.find()
		if(areas === null){
			util.throwErreur(404, "aucune area");
		}

		const areasHATEOAS = areas.map(area => {
			return{
				...area._doc,
				_links: [{
					self: {
						method: "GET",
						href: url_base + req.url + "/" + area._id.toString()
					},
					update: {
						method: "PUT",
						href: url_base + req.url + "/" + area._id.toString()
					},
					delete: {
						method: "DELETE",
						href: url_base + req.url + "/" + area._id.toString()
					}
				}]
			}
		})

		res.status(200).json(areasHATEOAS);

	} catch (err) {
		next(err);
	}
};

exports.getUserAreas = async (req, res, next) => {
	Area.find({ user: req.user.id })
		.then(area => {
			res.status(200).json(area);
		})
		.catch(err => {
			next(err);
		});
};

exports.getArea = async (req, res, next) => {
	Area.findById(req.params.areaId)
		.then(area => {
			util.validateNull(area, "zone innexistante", 404);
			res.status(200).json(area);
		})
		.catch(err => {
			next(err);
		});

};

exports.updateArea = async (req, res, next) => {
	try {
		const { name, description, gettingThere, lon, lat, routes } = req.body
		const user = req.user.id
		const area = await Area.findByIdAndUpdate(req.params.areaId, { name: name, description: description, gettingThere: gettingThere, lon: lon, lat: lat, routes: routes, user: user }, { new: true })
		util.validateNull(area, "area inexistante", 404);
		res.status(200).json(area);
	}
	catch (err) {
		next(err);
	}
}

exports.deleteArea = async (req, res, next) => {
	try {
		const area = await Area.deleteOne({ _id: req.params.areaId })
		util.validateNull(area, "zone inexistante", 404);
		res.status(204).json(area);
	}
	catch (err) {
		next(err);
	}

}

exports.getRoutes = async (req, res, next) => {
	try {
		const area = await Area.findOne({ _id: req.params.areaId });
		util.validateNull(area, "area inexistant", 404);
		Route.find({ area: req.params.areaId })
			.then(routes => {
				util.validateNull(routes, "routes inexistante", 404);
				res.status(200).json(routes);
			})
	}
	catch (err) {
		next(err);
	}
};


