const User = require("../models/user");
const util = require("../controllers/utils");


exports.validateArea = async (req, res, next) => {

    try {
        const { name, description, gettingThere, lon, lat } = req.body
        const user = req.user.id
        const testUser = await User.exists({ _id: user });
        util.validateNull(testUser, "utilisateur inexistant", 422);

        if (name.length <= 0)
            util.throwErreur(422, "nom invalide");
        else if (description.length <= 0)
            util.throwErreur(422, "description invalide");
        else if (gettingThere.length <= 0)
            util.throwErreur(422, "se rendre invalide");
        else if (lon < -180 || lon > 180)
            util.throwErreur(422, "longitude invalide");
        else if (lat < -90 || lat > 90)
            util.throwErreur(422, "latitude invalide");
        next()
    }
    catch (err) {
        next(err)
    }
} 