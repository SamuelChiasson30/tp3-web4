const Area = require("../models/area");
const User = require("../models/user");
const util = require("../controllers/utils");

exports.validateRoute = async (req, res, next) => {

    try {
        const { name, type, grade, description, approach, descent, area} = req.body
		const user = req.user.id
        const testUser = await User.exists({ _id: user });
		const testArea = await Area.exists({ _id: area });
		util.validateNull(testUser, "utilisateur inexistant", 422);
		util.validateNull(testArea, "area inexistante", 422);

        if(!type)
			util.throwErreur(422, "type absent");
        if(type != "SPORT" && type != "TRAD" && type != "TOPROPE")
			util.throwErreur(422, "type invalide");
		if(!grade)
			util.throwErreur(422, "grade absent");
        if(grade.value < 5.07|| grade.value > 5.154)
            util.throwErreur(422, "grade invalide");
        if(!grade.text)
            util.throwErreur(422, "grade invalide");
		if (name.length <= 0)
			util.throwErreur(422, "nom invalide");
		else if (description.length <= 0)
			util.throwErreur(422, "description invalide");
        next()
    }
    catch (err) {
        next(err)
    }
} 