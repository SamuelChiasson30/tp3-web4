
const areas = [
    {
        "_id": "6627cd8f9cf3928ae0db3a36",
        "name": "area 1",
        "description": "description 1",
        "gettingThere": "gettingThere 1",
        "lon": 10,
        "lat": 10,
        "routes": ["6627cb17e03a27a7beaabc71", "6627cbbbf1531b9661ba3b0f", "6627cbc2bd63616f71c93599", "6627cbca12264fdafb66e4e6", "6627cbd0ebbfbeb235486155"],
        "user": "630f5b8b8b8b8b8b8b8b8b1a"

    },
    {
        "_id": "6627d8bc380b39aaa8d571ef",
        "name": "area 2",
        "description": "description 2",
        "gettingThere": "gettingThere 2",
        "lon": 20,
        "lat": 20,
        "routes": ["6627cbd65fd6e5723da8de95", "6627cbdde86a3cb3933516fd", "6627cbe5af8644223fea9934", "6627cbf1dd860927900d0f3c", "6627cbf65a4c8b9977dd367a"],
        "user": "630f5b8b8b8b8b8b8b8b8b1a"

    },
    {
        "_id": "6627d8cfa001b674cdda19a3",
        "name": "area 3",
        "description": "description 3",
        "gettingThere": "gettingThere 3",
        "lon": 30,
        "lat": 30,
        "routes": ["6627cbfd6e66a2e5bd949bb5", "6627cc0291fd9cabfff67a22", "6627cc0ddcbadb7625abe2fb", "6627cc131ed51811514b43b6", "6627cc1b43e5b6070de3e47c"],
        "user": "631f5b8b8b8b8b8b8b8b8b1b"

    },
    {
        "_id": "6627d8d829a5cf1e0dfec73b",
        "name": "area 4",
        "description": "description 4",
        "gettingThere": "gettingThere 4",
        "lon": 40,
        "lat": 40,
        "routes": ["6627cc210e148dfd4b030d4a", "6627cc275287ecc052baac90", "6627cc2cd48be1bbb00a0dac", "6627cc3374c781300e492310", "6627cc3ad067a3ab7e20f466"],
        "user": "631f5b8b8b8b8b8b8b8b8b1b"

    },
]


module.exports = areas;
