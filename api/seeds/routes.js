
const routes = [
    {
        "_id": "6627cb17e03a27a7beaabc71",
        "name": "route 1",
        "type": "TRAD",
        "grade": {
            "text": "5.8",
            "value": 5.08,
        },
        "description": "description route 1",
        "approach": "approche route 1",
        "descent": "descente route 1",
        "area": "6627cd8f9cf3928ae0db3a36",
        "user": "630f5b8b8b8b8b8b8b8b8b1a"
    },
    {
        "_id": "6627cbbbf1531b9661ba3b0f",
        "name": "route 2",
        "type": "TRAD",
        "grade": {
            "text": "5.9",
            "value": 5.09,
        },
        "description": "description route 2",
        "approach": "approche route 2",
        "descent": "descente route 2",
        "area": "6627cd8f9cf3928ae0db3a36",
        "user": "630f5b8b8b8b8b8b8b8b8b1a"
    },
    {
        "_id": "6627cbc2bd63616f71c93599",
        "name": "route 3",
        "type": "TRAD",
        "grade": {
            "text": "5.10a",
            "value": 5.101,
        },
        "description": "description route 3",
        "approach": "approche route 3",
        "descent": "descente route 3",
        "area": "6627cd8f9cf3928ae0db3a36",
        "user": "630f5b8b8b8b8b8b8b8b8b1a"
    },
    {
        "_id": "6627cbca12264fdafb66e4e6",
        "name": "route 4",
        "type": "TRAD",
        "grade": {
            "text": "5.11b",
            "value": 5.112,
        },
        "description": "description route 4",
        "approach": "approche route 4",
        "descent": "descente route 4",
        "area": "6627cd8f9cf3928ae0db3a36",
        "user": "630f5b8b8b8b8b8b8b8b8b1a"
    },
    {
        "_id": "6627cbd0ebbfbeb235486155",
        "name": "route 5",
        "type": "TRAD",
        "grade": {
            "text": "5.8",
            "value": 5.08,
        },
        "description": "description route 5",
        "approach": "approche route 5",
        "descent": "descente route 5",
        "area": "6627cd8f9cf3928ae0db3a36",
        "user": "630f5b8b8b8b8b8b8b8b8b1a"
    },
    {
        "_id": "6627cbd65fd6e5723da8de95",
        "name": "route 6",
        "type": "TRAD",
        "grade": {
            "text": "5.9",
            "value": 5.09,
        },
        "description": "description route 6",
        "approach": "approche route 6",
        "descent": "descente route 6",
        "area": "6627d8bc380b39aaa8d571ef",
        "user": "630f5b8b8b8b8b8b8b8b8b1a"
    },
    {
        "_id": "6627cbdde86a3cb3933516fd",
        "name": "route 7",
        "type": "TRAD",
        "grade": {
            "text": "5.11b",
            "value": 5.112,
        },
        "description": "description route 7",
        "approach": "approche route 7",
        "descent": "descente route 7",
        "area": "6627d8bc380b39aaa8d571ef",
        "user": "630f5b8b8b8b8b8b8b8b8b1a"
    },
    {
        "_id": "6627cbe5af8644223fea9934",
        "name": "route 8",
        "type": "TRAD",
        "grade": {
            "text": "5.10a",
            "value": 5.101,
        },
        "description": "description route 8",
        "approach": "approche route 8",
        "descent": "descente route 8",
        "area": "6627d8bc380b39aaa8d571ef",
        "user": "630f5b8b8b8b8b8b8b8b8b1a"
    },
    {
        "_id": "6627cbf1dd860927900d0f3c",
        "name": "route 9",
        "type": "TRAD",
        "grade": {
            "text": "5.11d",
            "value": 5.114,
        },
        "description": "description route 9",
        "approach": "approche route 9",
        "descent": "descente route 9",
        "area": "6627d8bc380b39aaa8d571ef",
        "user": "630f5b8b8b8b8b8b8b8b8b1a"
    },
    {
        "_id": "6627cbf65a4c8b9977dd367a",
        "name": "route 10",
        "type": "TRAD",
        "grade": {
            "text": "5.12d",
            "value": 5.124,
        },
        "description": "description route 10",
        "approach": "approche route 10",
        "descent": "descente route 10",
        "area": "6627d8bc380b39aaa8d571ef",
        "user": "630f5b8b8b8b8b8b8b8b8b1a"
    },
    {
        "_id": "6627cbfd6e66a2e5bd949bb5",
        "name": "route 11",
        "type": "TRAD",
        "grade": {
            "text": "5.13d",
            "value": 5.134,
        },
        "description": "description route 11",
        "approach": "approche route 11",
        "descent": "descente route 11",
        "area": "6627d8cfa001b674cdda19a3",
        "user": "631f5b8b8b8b8b8b8b8b8b1b"
    },
    {
        "_id": "6627cc0291fd9cabfff67a22",
        "name": "route 12",
        "type": "TRAD",
        "grade": {
            "text": "5.8",
            "value": 5.08,
        },
        "description": "description route 12",
        "approach": "approche route 12",
        "descent": "descente route 12",
        "area": "6627d8cfa001b674cdda19a3",
        "user": "631f5b8b8b8b8b8b8b8b8b1b"
    },
    {
        "_id": "6627cc0ddcbadb7625abe2fb",
        "name": "route 13",
        "type": "TRAD",
        "grade": {
            "text": "5.7",
            "value": 5.07,
        },
        "description": "description route 13",
        "approach": "approche route 13",
        "descent": "descente route 13",
        "area": "6627d8cfa001b674cdda19a3",
        "user": "631f5b8b8b8b8b8b8b8b8b1b"
    },
    {
        "_id": "6627cc131ed51811514b43b6",
        "name": "route 14",
        "type": "TRAD",
        "grade": {
            "text": "5.9",
            "value": 5.09,
        },
        "description": "description route 14",
        "approach": "approche route 14",
        "descent": "descente route 14",
        "area": "6627d8cfa001b674cdda19a3",
        "user": "631f5b8b8b8b8b8b8b8b8b1b"
    },
    {
        "_id": "6627cc1b43e5b6070de3e47c",
        "name": "route 15",
        "type": "TRAD",
        "grade": {
            "text": "5.7",
            "value": 5.07,
        },
        "description": "description route 15",
        "approach": "approche route 15",
        "descent": "descente route 15",
        "area": "6627d8cfa001b674cdda19a3",
        "user": "631f5b8b8b8b8b8b8b8b8b1b"
    },
    {
        "_id": "6627cc210e148dfd4b030d4a",
        "name": "route 16",
        "type": "TRAD",
        "grade": {
            "text": "5.8",
            "value": 5.08,
        },
        "description": "description route 16",
        "approach": "approche route 16",
        "descent": "descente route 16",
        "area": "6627d8d829a5cf1e0dfec73b",
        "user": "631f5b8b8b8b8b8b8b8b8b1b"
    },
    {
        "_id": "6627cc275287ecc052baac90",
        "name": "route 17",
        "type": "TRAD",
        "grade": {
            "text": "5.7",
            "value": 5.07,
        },
        "description": "description route 17",
        "approach": "approche route 17",
        "descent": "descente route 17",
        "area": "6627d8d829a5cf1e0dfec73b",
        "user": "631f5b8b8b8b8b8b8b8b8b1b"
    },
    {
        "_id": "6627cc2cd48be1bbb00a0dac",
        "name": "route 18",
        "type": "TRAD",
        "grade": {
            "text": "5.10a",
            "value": 5.101,
        },
        "description": "description route 18",
        "approach": "approche route 18",
        "descent": "descente route 18",
        "area": "6627d8d829a5cf1e0dfec73b",
        "user": "631f5b8b8b8b8b8b8b8b8b1b"
    },
    {
        "_id": "6627cc3374c781300e492310",
        "name": "route 19",
        "type": "TRAD",
        "grade": {
            "text": "5.9",
            "value": 5.09,
        },
        "description": "description route 19",
        "approach": "approche route 19",
        "descent": "descente route 19",
        "area": "6627d8d829a5cf1e0dfec73b",
        "user": "631f5b8b8b8b8b8b8b8b8b1b"
    },
    {
        "_id": "6627cc3ad067a3ab7e20f466",
        "name": "route 20",
        "type": "TRAD",
        "grade": {
            "text": "5.8",
            "value": 5.08,
        },
        "description": "description route 20",
        "approach": "approche route 20",
        "descent": "descente route 20",
        "area": "6627d8d829a5cf1e0dfec73b",
        "user": "631f5b8b8b8b8b8b8b8b8b1b"
    },
]


module.exports = routes;
