"use strict";

const express = require("express");

const router = express.Router();

const routeController = require("../controllers/routeController");
const isAuth = require("../middleware/is-auth");
const validationRoute = require("../middleware/routeValidation");



// /routes/ => POST
router.post("/routes/",isAuth, validationRoute.validateRoute, routeController.createRoute);

// /routes/ => GET
router.get("/routes/", routeController.getRoutes);

// /routes/my-routes => GET
router.get("/routes/my-routes",isAuth, routeController.getUserRoutes);

// /routes/:routeId => GET
router.get("/routes/:routeId", routeController.getRoute);

// /routes/:routeId => PUT
router.put("/routes/:routeId", isAuth, validationRoute.validateRoute, routeController.updateRoute);

// /routes/:routeId => DELETE
router.delete("/routes/:routeId",  routeController.deleteRoute);

module.exports = router;

