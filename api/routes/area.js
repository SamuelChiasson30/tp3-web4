"use strict";

const express = require("express");

const router = express.Router();

const areaController = require("../controllers/areaController");
const isAuth = require("../middleware/is-auth");
const validateArea = require("../middleware/areaValidation");


// /areas/ => POST
router.post("/areas/", isAuth, validateArea.validateArea, areaController.createArea);

// /areas/ => GET
router.get("/areas/", areaController.getAreas);

// /areas/my-areas => GET
router.get("/areas/my-areas",isAuth, areaController.getUserAreas);

// /areas/:areaId => GET
router.get("/areas/:areaId", areaController.getArea);

// /areas/:areaId/routes => GET
router.get("/areas/:areaId/routes", areaController.getRoutes);

// /areas/:areaId => PUT
router.put("/areas/:areaId",isAuth, validateArea.validateArea, areaController.updateArea);

// /areas/:areaId => DELETE
router.delete("/areas/:areaId", areaController.deleteArea);

module.exports = router;

